package search;

import java.util.HashSet;
import java.util.Set;

import search.nodes.Node;

/**
 * Same as a tree search, but with a hashmap-backed set
 * to make sure we don't repeat states.
 * @author ms13vg
 *
 */
public class GraphSearch extends Search {
	private final Set<State> visitedStates = new HashSet<State>(); 

	public GraphSearch(Frontier frontier) {
		super(frontier);
	}

	/**
	 * The same code as before but with a hashmap to avoid repetitions
	 * of states
	 */
	@Override
	public Node run(Node root) {
		frontier.push(root);
		nodesCreated++;
		visitedStates.add(root.state);
		
		while(!frontier.isEmpty()) {
			Node node = frontier.pop();
			if(node.state.isGoal()) 
				return onExit(node);
			else {
				Set<Action> possibleActions = node.state.getActions();
				for(Action a : possibleActions) {
					State nextState = node.state.makeTransition(a);
					
					//If the new state is not already visited
					if(!visitedStates.contains(nextState)) {
						frontier.push(new Node(node, a, nextState));
						nodesCreated++;
						visitedStates.add(nextState);
					}
					
					//Else it is, and then we just ignore it
				}
			}
		}
		
		return onExit(null);
	}
	
	/**
	 * Clear the states set as well
	 */
	@Override
	public Node onExit(Node n) {
		visitedStates.clear();
		return super.onExit(n);
	}

}
