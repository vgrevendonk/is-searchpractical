package search.depthfirst;

import java.util.Stack;

import search.Frontier;
import search.nodes.Node;

/**
 * DFS is based on a stack. Therefore this frontier should
 * be implemented that way.
 * @author ms13vg
 *
 */
public class DepthFirstFrontier implements Frontier {
	private final Stack<Node> nodes = new Stack<>();

	@Override
	public void clear() {
		nodes.clear();
	}

	@Override
	public boolean isEmpty() {
		return nodes.isEmpty();
	}

	@Override
	public Node pop() {
		return nodes.pop();
	}

	@Override
	public void push(Node n) {
		nodes.push(n);
	}

	@Override
	public int size() {
		return nodes.size();
	}

}
