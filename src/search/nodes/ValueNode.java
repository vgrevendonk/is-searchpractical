package search.nodes;

import search.Action;
import search.State;

public class ValueNode extends Node {
	public int value = 0;
	/**
	 * The cost to get to this node
	 */
	public int cost;
	
	public ValueNode(Node parent, Action action, State state, int cost) {
		super(parent, action, state);
		this.cost = cost;
	}

	public ValueNode(Node parent, Action action, State state, int cost, int value) {
		super(parent, action, state);
		this.value = value;
		this.cost = cost;
	}
	
	public ValueNode(Node fromNode) {
		super(fromNode.parent, fromNode.action, fromNode.state);
		if(fromNode.parent != null)
			this.cost = ((ValueNode)fromNode.parent).cost + fromNode.action.cost();
		else
			this.cost = 0;
	}
}
