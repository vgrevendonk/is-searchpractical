package search.nodes;

import search.Action;
import search.State;

/**
 * A node extension for the Iterative Deepening Graph Search
 * @author ms13vg
 *
 */
public class IDTSNode extends Node {
	public final int depth;

	public IDTSNode(Node parent, Action action, State state, int depth) {
		super(parent, action, state);
		this.depth = depth;
	}
	
	public IDTSNode(Node node, int depth) {
		super(node.parent, node.action, node.state);
		this.depth = depth;
	}
}
