package search.bestfirst;

import java.util.Comparator;
import java.util.PriorityQueue;

import search.Frontier;
import search.NodeFunction;
import search.nodes.Node;
import search.nodes.ValueNode;

public class BestFirstFrontier implements Frontier, Comparator<ValueNode> {
	private final NodeFunction f;
	private final PriorityQueue<ValueNode> queue;
	
	/**
	 * Create a priority queue with this as a comparator
	 * @param f
	 */
	public BestFirstFrontier(NodeFunction f) {
		this.f = f;
		this.queue = new PriorityQueue<>(100, this);
	}

	@Override
	public void clear() {
		queue.clear();
	}

	@Override
	public boolean isEmpty() {
		return queue.isEmpty();
	}

	@Override
	public Node pop() {
		return queue.poll();
	}

	@Override
	public void push(Node n) {
		ValueNode vn = new ValueNode(n);
		
		vn.value = f.f(vn); //Very ugly
		
		queue.offer(vn);
	}

	@Override
	public int compare(ValueNode o1, ValueNode o2) {
		return o1.value - o2.value;
	}

	@Override
	public int size() {
		return queue.size();
	}

}
