package search.bestfirst;

import search.NodeFunction;
import search.nodes.ValueNode;

public class AStarFunction implements NodeFunction {
	private final NodeFunction heuristic;
	
	public AStarFunction(NodeFunction heuristic) {
		this.heuristic = heuristic;
	}

	@Override
	public int f(ValueNode n) {
		return n.cost + heuristic.f(n); //UGLYYYYY
	}

}
