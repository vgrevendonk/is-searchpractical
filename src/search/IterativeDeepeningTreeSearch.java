package search;

import java.util.Set;

import search.depthfirst.DepthFirstFrontier;
import search.nodes.IDTSNode;
import search.nodes.Node;

public class IterativeDeepeningTreeSearch extends Search {
	private int currentDepthLimit = 0;

	public IterativeDeepeningTreeSearch() {
		super(new DepthFirstFrontier());
	}

	@Override
	public Node run(Node root) {
		for(currentDepthLimit = 1; ;currentDepthLimit++) {
			frontier.push(new IDTSNode(root, 0));
			nodesCreated++;
			
			while(!frontier.isEmpty()) {
				IDTSNode node = (IDTSNode) frontier.pop();
				if(node.state.isGoal()) 
					return onExit(node);
				else if(node.depth < currentDepthLimit) {
					Set<Action> possibleActions = node.state.getActions();
					for(Action a : possibleActions) {
						frontier.push(new IDTSNode(node, a, node.state.makeTransition(a), node.depth+1));
						nodesCreated++;
					}
				}
			}
		}
	}
}
