package search;

import search.nodes.Node;


/**
 * A straight translation of the constraints stated
 * in part 2 of practical 2.
 * @author ms13vg
 *
 */
public interface Frontier {
	public void clear();
	public boolean isEmpty();
	/**
	 * Retrieve the next node on the list.
	 * @return
	 */
	public Node pop();
	/**
	 * Add a node n to the frontier
	 * @param n
	 */
	public void push(Node n);
	
	/**
	 * The current size of this frontier
	 * @return
	 */
	public int size();
}
