package search;

/**
 * Do not forget to implement a toString method when implementing
 * this interface!
 * @author ms13vg
 *
 */
public interface Action {
	public int cost();
}
