package search;

import java.util.Set;

/**
 * Do not forget to implement a toString method when implementing
 * this interface!
 * @author ms13vg
 *
 */
public interface State extends GoalTest {
	public Set<Action> getActions();
	public State makeTransition(Action a);
	
	//Practical 2
	//Declaring these methods in this interface is not entirely
	// justified; in fact their declaration has no impact at all.
	// This is due to the fact every object in java has already
	// an implementation for this.
	public boolean equals(Object o);
	public int hashCode();	
}
