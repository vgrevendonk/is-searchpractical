package search;

import search.nodes.Node;

/**
 * This interface was turned into an abstract class to avoid code replication
 * according to the DRY principle.
 * @author ms13vg
 *
 */
public abstract class Search {
	protected final Frontier frontier;
	
	//Statistics
	protected int nodesCreated = 0;
	private int maxFrontierSize = 0;
	
	public Search(Frontier frontier) {
		this.frontier = frontier;
	}
	
	/**
	 * Note that this method does not take a goal test, since this
	 * is included in the state definition.
	 * @param root
	 * @return
	 */
	public abstract Node run(Node root);
	
	/**
	 * Avoid code duplication: some actions to be made when 
	 * the search is done
	 * @param n
	 * @return
	 */
	protected Node onExit(Node n) {
		maxFrontierSize = frontier.size();
		frontier.clear();
		return n;
	}
	
	/**
	 * 
	 */
	public void printStatistics() {
		System.out.println("During computation, these were the statistics:");
		System.out.println("Number of nodes generated: " + nodesCreated);
		
		System.out.println("Number of nodes in frontier: " + maxFrontierSize);
	}
}
