package search.breadthfirst;

import java.util.LinkedList;
import java.util.Queue;

import search.Frontier;
import search.nodes.Node;

/**
 * A BFS is based on a queue datastructure.
 * @author ms13vg
 *
 */
public class BreadthFirstFrontier implements Frontier {
	private final Queue<Node> nodes = new LinkedList<>();

	@Override
	public void clear() {
		nodes.clear();
	}

	@Override
	public boolean isEmpty() {
		return nodes.isEmpty();
	}

	@Override
	public Node pop() {
		return nodes.poll();
	}

	@Override
	public void push(Node n) {
		nodes.offer(n);
	}

	@Override
	public int size() {
		return nodes.size();
	}

}
