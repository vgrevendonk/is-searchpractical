package search;

import java.util.Set;

import search.nodes.Node;

public class TreeSearch extends Search {

	public TreeSearch(Frontier frontier) {
		super(frontier);
	}

	/**
	 * This code is shamefully the same as in BFTS...
	 */
	@Override
	public Node run(Node root) {
		frontier.push(root);
		nodesCreated++;
		
		while(!frontier.isEmpty()) {
			Node node = frontier.pop();
			if(node.state.isGoal()) 
				return onExit(node);
			else {
				Set<Action> possibleActions = node.state.getActions();
				for(Action a : possibleActions) {
					frontier.push(new Node(node, a, node.state.makeTransition(a)));
					nodesCreated++;
				}
			}
		}
		
		return onExit(null);
	}
}
