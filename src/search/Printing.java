package search;

import java.util.Stack;

import search.nodes.Node;


/**
 * <p>Print the solution for a general search problem.</p>
 * <p>For the sake of clarity this class has been kept
 * as general as possible: it requires State and Action to 
 * implement a toString method. This allows this class
 * to be used out-of-the-box.</p>
 * @author ms13vg
 *
 */
public class Printing {
	public void printSolution(Node solution) {
		if (solution == null)
			System.out.println("No solution found!");
		else {
			Stack<Node> stack = new Stack<Node>();
			Node node = solution;
			do {
				stack.push(node);
			} while((node = node.parent) != null);

			int stepNo = 0;
			while (!stack.isEmpty()) {
				node = stack.pop();
				System.out.print(stepNo++);
				if (node.parent == null)
					System.out.println(": start");
				else {
					System.out.print(": ");
					System.out.println(node.action);
				}
				System.out.println(node.state);
			}
		}
	}
	
	protected static void printChar(char c, int number) {
		for (int index = 0; index < number; index++)
			System.out.print(c);
	}
	
	//Abstract methods "print" have been replace by toString methods.
}
