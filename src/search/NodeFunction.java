package search;

import search.nodes.ValueNode;

public interface NodeFunction {
	public int f(ValueNode n);
}
