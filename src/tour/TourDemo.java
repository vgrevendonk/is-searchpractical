package tour;

import search.GraphSearch;
import search.IterativeDeepeningTreeSearch;
import search.Printing;
import search.Search;
import search.TreeSearch;
import search.bestfirst.AStarFunction;
import search.bestfirst.BestFirstFrontier;
import search.breadthfirst.BreadthFirstFrontier;
import search.depthfirst.DepthFirstFrontier;
import search.nodes.Node;


public class TourDemo {
	public static final boolean SHOW_ANSWER = false;
	public static final boolean SHOW_STATS = true;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Cities romania = SetUpRomania.getRomaniaMapSmall();
		
		test(new TreeSearch(new BreadthFirstFrontier()), "BFTS", romania);
		test(new GraphSearch(new BreadthFirstFrontier()), "BFGS", romania);
		//test(new TreeSearch(new DepthFirstFrontier()), "DFTS", romania);
		//test(new GraphSearch(new DepthFirstFrontier()), "DFGS", romania);
		//test(new IterativeDeepeningTreeSearch(), "IDTS", romania);
		
		//A*
		//test(new GraphSearch(new BestFirstFrontier(new AStarFunction(new CityDistanceHeuristicNodeFunction()))), "ASGS", romania);
		//test(new TreeSearch(new BestFirstFrontier(new AStarFunction(new CityDistanceHeuristicNodeFunction()))), "ASTS", romania);
	}
	
	public static void test(Search algorithm, String name, Cities romania) {
		System.out.println("Demonstration: " + name);
		
		TourState initialConfiguration = new TourState(romania, "Arad");
		
		Node solution = algorithm.run(new Node(null, null, initialConfiguration));
		if(SHOW_ANSWER)
			new Printing().printSolution(solution);
		if(SHOW_STATS)
			algorithm.printStatistics();
		
		System.out.println("------------------------------------------");
		System.out.println("------------------------------------------");
	}

}
