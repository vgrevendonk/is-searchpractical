package tour;

import search.NodeFunction;
import search.nodes.ValueNode;

public class CityDistanceHeuristicNodeFunction implements NodeFunction {

	@Override
	public int f(ValueNode n) {
		return ((TourState) n.state).heuristic1();
	}

}
