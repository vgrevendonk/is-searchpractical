package tour;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import search.Action;
import search.State;

public class TourState implements State {
	private final ArrayList<City> visitedCities;
	private final Cities cityList;
	
	public TourState(Cities cities, String startCity) {
		cityList = cities;
		visitedCities = new ArrayList<>();
		visitedCities.add(cities.citiesByName.get(startCity));
	}
	
	public TourState(ArrayList<City> previouslyVisited, Cities cities) {
		cityList = cities;
		visitedCities = previouslyVisited;
	}
	
	@Override
	/**
	 * Check if every city has been visited.
	 */
	public boolean isGoal() {
		return visitedCities.containsAll(cityList.getAllCities()) 
				&& visitedCities.get(0) == visitedCities.get(visitedCities.size()-1);
	}
	
	/**
	 * 
	 */
	@Override
	public Set<Action> getActions() {
		LinkedHashSet<Action> actions = new LinkedHashSet<>();
		for(Road r : getCurrentCity().outgoingRoads) {
			actions.add(r);
		}
		
		return actions;
	}
	@Override
	public State makeTransition(Action a) {
		Road chosenRoad = (Road) a;
		
		//Copy city list for new state
		ArrayList<City> visitedCitiesCopy = new ArrayList<>();
		visitedCitiesCopy.addAll(visitedCities);
		visitedCitiesCopy.add(chosenRoad.targetCity);
		
		return new TourState(visitedCitiesCopy, cityList);
	}
	@Override
	public String toString() {
		String out = "";
		
		for(City c : visitedCities) {
			out += c.getName() + "\n -> ";
		}
		
		if(this.isGoal())
			out += "DONE";
		else
			out += "X";
		
		return out;
	}
	
	private City getCurrentCity() {
		return visitedCities.get(visitedCities.size()-1);
	}
 
	/**
	 * get distance to furthest city to travel to, added to the distance 
	 * to go back to the goal (ie the initial city)
	 * @return
	 */
	public int heuristic1() {
		ArrayList<City> cities = new ArrayList<>(cityList.getAllCities());
		cities.removeAll(visitedCities);
		
		int max = -1;
		City furthest = getCurrentCity();
		City current = getCurrentCity();
		
		for(City c : cities) {
			int d = c.getShortestDistanceTo(current);
			if(d > max) {
				max = d;
				furthest = c;
			}
		}
		
		return max + furthest.getShortestDistanceTo(visitedCities.get(0));
	}
	
	//Practical 2
	public ArrayList<City> getVisitedCities() {
		return visitedCities;
	}

	@Override
	/**
	 * Compare the visited cities list.
	 * City comparison is OK because there is only a limited
	 * number of city instances (they are unique).
	 */
	public boolean equals(Object o) {
		/*try {
			TourState s = (TourState) o;
			List<City> cities = s.getVisitedCities();
			for(int i = 0; i < visitedCities.size(); i++) {
				if(visitedCities.get(i) != cities.get(i))
					return false;
			}
			
			return true;
		} catch(ClassCastException e) {
			return false;
		}*/
		
		if(this == o) {
			return true;
		}
		if(!(o instanceof TourState)) {
			return false;
		} 
		TourState s = (TourState) o;
		return visitedCities.equals(s.visitedCities) && getCurrentCity().equals(s.getCurrentCity());
	}
	
	@Override
	public int hashCode() {
		/*StringBuffer sb = new StringBuffer();
		for(City c: visitedCities) {
			sb.append(c.name);
		}
		
		return sb.toString().hashCode();*/
		
		return visitedCities.hashCode()*7 + getCurrentCity().hashCode();
	}
}
