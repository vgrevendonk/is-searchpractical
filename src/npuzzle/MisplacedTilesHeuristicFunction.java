package npuzzle;

import search.NodeFunction;
import search.nodes.ValueNode;

/**
 * Will return the number of misplaced tiles when called
 * @author ms13vg
 *
 */
public class MisplacedTilesHeuristicFunction implements NodeFunction {

	/**
	 * This is horrendously ugly
	 */
	@Override
	public int f(ValueNode n) {
		return ((Tiles) n.state).distanceFromGoal();
	}

}
