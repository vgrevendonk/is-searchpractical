package npuzzle;

import search.GraphSearch;
import search.IterativeDeepeningTreeSearch;
import search.Printing;
import search.Search;
import search.TreeSearch;
import search.bestfirst.AStarFunction;
import search.bestfirst.BestFirstFrontier;
import search.breadthfirst.BreadthFirstFrontier;
import search.depthfirst.DepthFirstFrontier;
import search.nodes.Node;

public class Demo {
	public static final boolean SHOW_ANSWER = false;
	public static final boolean SHOW_STATS = true;
	
	public static void main(String[] args) {
		System.out.println("8-puzzle with different algorithms");
		System.out.println();
		
		test(new TreeSearch(new BreadthFirstFrontier()), "BFTS");
		test(new GraphSearch(new BreadthFirstFrontier()), "BFGS");
		//test(new TreeSearch(new DepthFirstFrontier()), "DFTS");
		//test(new GraphSearch(new DepthFirstFrontier()), "DFGS");
		test(new IterativeDeepeningTreeSearch(), "IDTS");
		
		//A*
		test(new TreeSearch(new BestFirstFrontier(new AStarFunction(new MisplacedTilesHeuristicFunction()))), "ASTS");
		test(new GraphSearch(new BestFirstFrontier(new AStarFunction(new MisplacedTilesHeuristicFunction()))), "ASGS");
	}
	
	public static void test(Search algorithm, String name) {
		System.out.println("Demonstration: " + name);
		
		Tiles initialConfiguration = new Tiles(new int[][] {
			{ 7, 4, 2 },
			{ 8, 1, 3 },
			{ 5, 0, 6 }
		});
		
		/*Tiles initialConfiguration = new Tiles(new int[][] {
			{ 3, 0 },
			{ 2, 1 }
		});*/
		
		Node solution = algorithm.run(new Node(null, null, initialConfiguration));
		
		if(SHOW_ANSWER)
			new Printing().printSolution(solution);
		if(SHOW_STATS)
			algorithm.printStatistics();
		
		System.out.println("------------------------------------------");
		System.out.println("------------------------------------------");
	}
}
